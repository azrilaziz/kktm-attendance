<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('is-admin', function ($user) {
            return in_array(1, $user->roles->pluck('id')->toArray());
        });

        Gate::define('is-lecturer', function ($user) {
            return in_array(2, $user->roles->pluck('id')->toArray());
        });

        Gate::define('is-student', function ($user) {
            return in_array(3, $user->roles->pluck('id')->toArray());
        });
    }
}
