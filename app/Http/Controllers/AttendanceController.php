<?php

namespace App\Http\Controllers;

use App\Models\Attendance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AttendanceController extends Controller
{
    public function attadmin()
    {
        $attendances = DB::select('SELECT b.fullname, b.identificationno AS nomatrik
        , b.ic, c.name AS room_name
        , d.name AS subject_name, d.subjectdate, d.subjecttimefrom
        , d.subjecttimeto, a.tagdatetime 
        from attendances AS a
        LEFT OUTER JOIN userdetails b ON (a.rfidno = b.rfidno)
        LEFT OUTER JOIN rooms c ON (a.deviceid = c.deviceid)
        LEFT OUTER JOIN subjects d ON (c.id = d.roomid)
        LEFT OUTER JOIN users e ON (b.id = e.userdetail_id)
        LEFT OUTER JOIN role_user f ON (e.id = f.user_id)');

        return view('dashboard', ['attendances' => $attendances]);
    }

    public function attlecturer()
    {
        $id = Auth::user()->id;

        $attendances = DB::select('SELECT a.id, b.fullname, b.identificationno AS nomatrik
        , b.ic, c.name AS room_name
        , d.name AS subject_name, d.subjectdate, d.subjecttimefrom
        , d.subjecttimeto, a.tagdatetime 
        from attendances AS a
        LEFT OUTER JOIN userdetails b ON (a.rfidno = b.rfidno)
        LEFT OUTER JOIN rooms c ON (a.deviceid = c.deviceid)
        LEFT OUTER JOIN subjects d ON (c.id = d.roomid)
        LEFT OUTER JOIN users e ON (b.id = e.userdetail_id)
        LEFT OUTER JOIN role_user f ON (e.id = f.user_id)
        WHERE f.role_id = 3 AND d.created_by = ?', [$id]);

        return view('dashboard', ['attendances' => $attendances]);
    }

    public function attstudent()
    {
        $id = Auth::user()->id;

        //dd($id);

        $attendances = DB::select('SELECT a.id
        , b.fullname
        , b.identificationno AS nomatrik
        , b.ic
        , c.name AS room_name
        , d.name AS subject_name
        , d.subjectdate
        , d.subjecttimefrom
        , d.subjecttimeto
        , a.tagdatetime 
        from attendances  AS a
        LEFT OUTER JOIN userdetails b ON (a.rfidno = b.rfidno)
        LEFT OUTER JOIN rooms c ON (a.deviceid = c.deviceid)
        LEFT OUTER JOIN subjects d ON (c.id = d.roomid)
        LEFT OUTER JOIN users e ON (b.id = e.userdetail_id)
        LEFT OUTER JOIN role_user f ON (e.id = f.user_id)
        WHERE f.role_id = 3 
        AND e.id = ?', [$id]);

        return view('dashboard', ['attendances' => $attendances]);
    }
}
