<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MemberController extends Controller
{
    //index page
    public function index()
    {
        return view('utama');
    }

    //login page
    public function login()
    {
        return view('member.login');
    }

    //register page
    public function register()
    {
        return view('member.register');
    }

    //profile page
    public function profile()
    {
        return view('member.profile');
    }

    //forgot password page
    public function forgotpassword()
    {
        return view('member.forgotpassword');
    }
}
