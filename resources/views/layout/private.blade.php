<!--begin::Header-->
@include('layout.header')
<!--end::Header-->
<!--begin::Wrapper-->
<div class="app-wrapper flex-column flex-row-fluid" id="kt_app_wrapper">
    <!--begin::Sidebar-->
    @include('layout.sidebar')
    <!--end::Sidebar-->
    <!--begin::Main-->
    <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
        @yield('content')
        @include('layout.footer')
    </div>
    <!--end:::Main-->
</div>
<!--end::Wrapper-->
</div>
<!--end::Page-->
</div>
<!--end::App-->

<!--begin::Javascript-->
<script>
    var hostUrl = "/template/assets/";
</script>
<!--begin::Global Javascript Bundle(mandatory for all pages)-->
<script src="/template/assets/plugins/global/plugins.bundle.js"></script>
<script src="/template/assets/js/scripts.bundle.js"></script>
<!--end::Global Javascript Bundle-->
<!--begin::Vendors Javascript(used for this page only)-->
<script src="/template/assets/plugins/custom/datatables/datatables.bundle.js"></script>
<script src="/template/assets/plugins/custom/vis-timeline/vis-timeline.bundle.js"></script>
<script src="https://cdn.amcharts.com/lib/5/index.js"></script>
<script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
<script src="https://cdn.amcharts.com/lib/5/percent.js"></script>
<script src="https://cdn.amcharts.com/lib/5/radar.js"></script>
<script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>
<!--end::Vendors Javascript-->
<!--begin::Custom Javascript(used for this page only)-->
<script src="/template/assets/js/widgets.bundle.js"></script>
<script src="/template/assets/js/custom/widgets.js"></script>
<script src="/template/assets/js/custom/apps/chat/chat.js"></script>
<script src="/template/assets/js/custom/utilities/modals/upgrade-plan.js"></script>
<script src="/template/assets/js/custom/utilities/modals/create-app.js"></script>
<script src="/template/assets/js/custom/utilities/modals/create-account.js"></script>
<script src="/template/assets/js/custom/utilities/modals/create-project/type.js"></script>
<script src="/template/assets/js/custom/utilities/modals/create-project/budget.js"></script>
<script src="/template/assets/js/custom/utilities/modals/create-project/settings.js"></script>
<script src="/template/assets/js/custom/utilities/modals/create-project/team.js"></script>
<script src="/template/assets/js/custom/utilities/modals/create-project/targets.js"></script>
<script src="/template/assets/js/custom/utilities/modals/create-project/files.js"></script>
<script src="/template/assets/js/custom/utilities/modals/create-project/complete.js"></script>
<script src="/template/assets/js/custom/utilities/modals/create-project/main.js"></script>
<script src="/template/assets/js/custom/utilities/modals/users-search.js"></script>
<!--end::Custom Javascript-->
<!--end::Javascript-->
</body>
<!--end::Body-->

</html>