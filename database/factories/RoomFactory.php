<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Room>
 */
class RoomFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => fake()->randomElement(['Computer Lab', 'Tutorial Room', 'Lecture Hall']),
            'building' => fake()->randomElement(['Block 1', 'Block 2', 'Block 3']),
            'floor' => fake()->numberBetween(1, 3)
        ];
    }
}
