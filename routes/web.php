<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\AttendanceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/contoh', function () {
	return view('contoh');
});

Route::get('/', function () {
	// return view('utama');
	return view('auth.login');
});

Route::get('/utama', function () {
	return view('utama');
});

Route::get('/member/login', [MemberController::class, 'login']);
Route::get('/member/register', [MemberController::class, 'register']);
Route::get('/member/forgotpassword', [MemberController::class, 'forgotpassword']);
Route::get('/member', [MemberController::class, 'index']);
Route::get('/member/profile', [MemberController::class, 'profile']);

// Route::view('dashboard', 'dashboard')
// 	->name('dashboard')
// 	->middleware(['auth']);



Route::middleware(['auth', 'can:is-admin'])->group(function () {

	Route::resource('admin/user', UserController::class);
	Route::resource('admin/room', RoomController::class);
	Route::resource('admin/role', RoleController::class);
	Route::get('dashboard', [AttendanceController::class, 'attadmin'])
		->name('dashboard')
		->middleware(['auth']);
});

Route::middleware(['auth', 'can:is-lecturer'])->group(function () {
	//Route::resource('admin/subject', SubjectController::class);
	Route::get('dashboard', [AttendanceController::class, 'attlecturer'])
		->name('dashboard')
		->middleware(['auth']);
});

Route::middleware(['auth', 'can:is-student'])->group(function () {
	Route::get('dashboard', [AttendanceController::class, 'attstudent'])
		->name('dashboard')
		->middleware(['auth']);
});



Route::get('classlist', function () {
	return view('/lecturer/class/index');
});
